<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Superheroes</h1>
<orly-table id="orlyTable" loaddataoncreate includefilter maxrows="10" tabletitle="Search Results" class="invisible" url="<%=request.getContextPath()%>/superheroes/getSuperheroesData">
	<orly-column field="id" label="Id" sorttype="natural"></orly-column>
	<orly-column field="realName" label="Real Name"></orly-column>
	<orly-column field="alias" label="Alias"></orly-column>
	<orly-column field="universe" label="Universe"></orly-column>
	<orly-column field="power" label="Power"></orly-column>
</orly-table>
