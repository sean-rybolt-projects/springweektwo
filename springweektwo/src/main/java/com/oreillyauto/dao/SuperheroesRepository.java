package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.SuperheroesRepositoryCustom;
import com.oreillyauto.domain.superheroes.Superhero;

public interface SuperheroesRepository extends CrudRepository<Superhero, Integer>, SuperheroesRepositoryCustom{

}
