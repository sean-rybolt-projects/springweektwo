package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.superheroes.Superhero;

public interface SuperheroesRepositoryCustom {
    
    public List<Superhero> getSuperheroes();
}
