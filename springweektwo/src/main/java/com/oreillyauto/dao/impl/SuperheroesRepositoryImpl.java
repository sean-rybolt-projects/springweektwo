package com.oreillyauto.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.SuperheroesRepositoryCustom;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.domain.superheroes.QSuperhero;
import com.oreillyauto.domain.superheroes.Superhero;


@Repository
public class SuperheroesRepositoryImpl extends QuerydslRepositorySupport implements SuperheroesRepositoryCustom {
    
    public SuperheroesRepositoryImpl() {
        super(Superhero.class);
        // TODO Auto-generated constructor stub
    }

    QSuperhero superheroTable = QSuperhero.superhero;

  
    @SuppressWarnings("unchecked")
    @Override
    public List<Superhero> getSuperheroes() {
        List<Superhero> mySuperList= (List<Superhero>) (Object) getQuerydsl().createQuery()
                .from(superheroTable)
                .fetch();
        return mySuperList;
    }
}
