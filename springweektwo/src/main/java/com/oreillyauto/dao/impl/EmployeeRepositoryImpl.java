package com.oreillyauto.dao.impl;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.EmployeeRepositoryCustom;
import com.oreillyauto.domain.examples.Employee;

@Repository
public class EmployeeRepositoryImpl extends QuerydslRepositorySupport implements EmployeeRepositoryCustom {
    public EmployeeRepositoryImpl() {
        super(Employee.class);
    }
}

