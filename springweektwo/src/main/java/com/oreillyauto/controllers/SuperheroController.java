package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.superheroes.Superhero;
import com.oreillyauto.service.SuperheroesService;

@Controller
public class SuperheroController {
    @Autowired
    SuperheroesService superheroService;

    @GetMapping(value = "/superheroes")
    public String getSuperheroes(Model model) {
        List<Superhero> superheroList = superheroService.getSuperheroes();
        for (Superhero superhero : superheroList) {
            System.out.println(superhero);
        }
        return "superheroes";
    }

    @ResponseBody
    @GetMapping(value = "/superheroes/getSuperheroesData")
    public List<Superhero> getSuperheroesData(Model model) {
        List<Superhero> superheroList = superheroService.getSuperheroes();
        for (Superhero superhero : superheroList) {
            System.out.println(superhero);
        }
        return superheroList;
    }
}
