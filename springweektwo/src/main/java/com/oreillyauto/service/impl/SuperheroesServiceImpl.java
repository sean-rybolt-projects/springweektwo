package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.SuperheroesRepository;
import com.oreillyauto.domain.superheroes.Superhero;
import com.oreillyauto.service.SuperheroesService;

@Service("superheroesService")
public class SuperheroesServiceImpl implements SuperheroesService{
    @Autowired 
    SuperheroesRepository superheroesRepo;
    
    public List<Superhero> getSuperheroes(){
        //(List<Superhero>) superheroesRepo.findAll()
        return superheroesRepo.getSuperheroes();
    }

}
