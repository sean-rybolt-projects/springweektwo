package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.superheroes.Superhero;

public interface SuperheroesService {
    public List<Superhero> getSuperheroes();
}
