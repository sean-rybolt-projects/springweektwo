package com.oreillyauto.domain.superheroes;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "superheroes")
public class Superhero implements Serializable {

    @Override
    public String toString() {
        return "Superhero [id=" + id + ", realName=" + realName + ", alias=" + alias + ", universe=" + universe + ", power=" + power + "]";
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "INTEGER")
    private Integer id;
    
    @Column(name = "real_name", columnDefinition = "VARCHAR(128)")
    private String realName;
    
    @Column(name = "alias", columnDefinition = "VARCHAR(128)")
    private String alias;

    @Column(name = "universe", columnDefinition = "VARCHAR(128)")
    private String universe;

    @Column(name = "power", columnDefinition = "VARCHAR(128)")
    private String power;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getUniverse() {
        return universe;
    }

    public void setUniverse(String universe) {
        this.universe = universe;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public Superhero() {}
    

}
